class Fixnum

  def in_words
    return "zero" if self.zero?
    evaluate(num_to_word_array(self))
  end

private

#adds power of 1000 to each string in array
def evaluate(array)
  word_array = []

  array.reverse.each_with_index do |string_number, idx|
    unless string_number == ""
      word_array << MAGNITUDE[idx] if MAGNITUDE[idx]
      word_array << string_number
    end
  end

  word_array.reverse.join(' ')
end

#returns an array of strings for each 3-digit block converted to words
def num_to_word_array(num)
  separate_blocks(num).map do |number|
    convert_block(number)
  end
end

#returns an array of each 3-digit block
def separate_blocks(num)
  no_blocks = blocks_count(num) - 1
  block_array = []

  no_blocks.downto(0).each do |count|
    block_array << num % (1000**(count+1)) / (1000**count)
  end

  block_array
end

#translates each 3-digit (000-999) block to words
def convert_block(num)
	hundreds_digit = num / 100
	tens = num % 100
	tens_digit = tens / 10 * 10
	ones_digit = tens % 10
	converted_block = []

	converted_block << "#{ONES[hundreds_digit]} hundred" unless
    hundreds_digit == 0
	if TEENS[tens]
		converted_block << TEENS[tens]
	else
		converted_block << "#{TENS[tens_digit]}" unless tens_digit == 0
		converted_block << "#{ONES[ones_digit]}" unless ones_digit == 0
	end

	converted_block.join(' ')
end

#determines count of how many 3-digit blocks exit in num
def blocks_count(num)
  count = 0
  while true
    count += 1
    break if num / 1000 < 1
    num /= 1000
  end

  return count
end

ONES = {
  1 => "one",
  2 => "two",
  3 => "three",
  4 => "four",
  5 => "five",
  6 => "six",
  7 => "seven",
  8 => "eight",
  9 => "nine"
}

TEENS = {
  10 => "ten",
  11 => "eleven",
  12 => "twelve",
  13 => "thirteen",
  14 => "fourteen",
  15 => "fifteen",
  16 => "sixteen",
  17 => "seventeen",
  18 => "eighteen",
  19 => "nineteen"
}

TENS = {
  20 => "twenty",
  30 => "thirty",
  40 => "forty",
  50 => "fifty",
  60 => "sixty",
  70 => "seventy",
  80 => "eighty",
  90 => "ninety"
}

MAGNITUDE = {
  1 => "thousand",
  2 => "million",
  3 => "billion",
  4 => "trillion",
  5 => "quadrillion"
}

end
